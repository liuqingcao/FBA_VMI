% The code below takes a matrix of projections and returns
% a matrix with filtered projections whose Fourier Transform
% is |w|.  By uncommenting the vairous filters, various aspects
% of the reconstructed picture will be emphasized or lost.

function ImageFilt = projfilter(Image)

[L,C]=size(Image);
w = [-pi : (2*pi)/L : pi-(2*pi)/L];

% Ramp filter (Linear High-Pass filter)
%  Filt = fftshift(abs(w)); % not work

% The opposite of the Ramp Function (Linear Low-Pass Filter)
Filt = (abs(w));

% N = length(yo);
% %Correction function
% f_corr = -2*abs((1:N)-(N/2+1))/N+1; % ramp filter


% - - - - - - - - - - - - - - - - -

% Below we use the Fourier Slice Theorem to filter the image
for i = 1:C
    IMG = fft(Image(:,i));
    FiltIMG = IMG.*Filt';
%     FiltIMG = filter (b, a, IMG);
    ImageFilt(:,i) = ifft(FiltIMG);
end

% Remove any remaining imaginary parts
ImageFilt = real(ImageFilt);