%
% function FBA_VMI_demo

%create the 3D momentum map
% source1 = create3DMomentum(100000,0  , 5 ,0 ,45 ,1,0.2);
% source2 = create3DMomentum(50000,120, 5 ,0 , 45,1,0.1);
% source3 = create3DMomentum(20000,-120,5 ,0 ,90 ,1,0.15);
% source = [source1;source2;source3];

source = create3DObject;


yMomentum = 0.2;
rotateStep = 3;
rotateRange = 180;
uniform = 1;
noiseLevel = 0;
yRes = 401;
zRes = 401;
yRange = [-1,1];
zRange = [-1,1];

%
figure('color',[1 1 1]);
subplot(241);
plot3(source(:,1),source(:,2),source(:,3),'.');
view(-30,20);
xlim([-1,1]);
ylim([-1,1]);
zlim([-1,1]);
% axis equal;
xlabel('x');
ylabel('y');
zlabel('z');
title('Momenta vector');

%binning the map
vex = linspace(-1,1,201);
vey = linspace(-1,1,101);
vez = linspace(-1,1,201);

[ N, ~ ] = histcnd( source, {vex;vey;vez});

subplot(242);
[~,index] = min(abs(vey-yMomentum));
imagesc(vex,vez,log10(squeeze(N(:,index,:))'));
axis equal;
set(gca,'Ydir','Normal');
xlabel('x');
ylabel('z');
xlim([-1,1]);
ylim([-1,1]);
title(['Momenta slice at y = ',num2str(yMomentum)]);



subplot(245);
imagesc(vey,vez,log10(squeeze(N(ceil(end/2)-1,:,:))'))
axis equal;
set(gca,'Ydir','Normal');
xlabel('y');
ylabel('z');
xlim([-1,1]);
ylim([-1,1]);
caxis([0,inf]);
title('Momenta slice at x = 0');
  
%project the 3D map onto MCP (y-z plane)
yAxis = linspace(min(yRange),max(yRange),yRes);
zAxis = linspace(min(zRange),max(zRange),zRes);
rotateTimes = ceil(rotateRange/rotateStep);
angleAxisOld = [0:rotateStep:(rotateTimes-1)*rotateStep];
%the rotate angle is not uniform
if(uniform)
    angleAxis = angleAxisOld;
else
    angleAxis(1) = angleAxisOld(1);
    angleAxis(rotateTimes) = angleAxisOld(rotateTimes);
    for i=2:rotateTimes-1
        angleAxis(i) = angleAxisOld(i) + rotateStep*rand(1,1);
    end
end
angleAxis
projectMap = zeros(rotateTimes,yRes,zRes);
for i=1:rotateTimes
    theta = angleAxis(i);
    sourceNow = (source * roty(theta,'deg'))';
    projectMap(i,:,:) = project2D(sourceNow,yAxis,zAxis,noiseLevel);
end
subplot(246);
imagesc(yAxis,zAxis,log10(squeeze(projectMap(1,:,:))))
axis equal;
set(gca,'Ydir','Normal');
xlabel('y');
ylabel('z');
xlim([-1,1]);
ylim([-1,1]);
caxis([0,inf]);
title('Momenta projection on YZ plane');


subplot(243);
[~,index] = min(abs(yAxis-0));
imagesc(angleAxis,zAxis,log10(squeeze(projectMap(:,:,index)))');
% axis equal;
set(gca,'Ydir','Normal');
xlabel('y');
ylabel('z');
ylim([-1,1]);
title(['Rotate at y = ',num2str(0)]);

subplot(247);
[~,index] = min(abs(yAxis-0));
data = squeeze(projectMap(:,:,index));
BackI = back(data', angleAxis);
imagesc(yAxis,zAxis,((BackI)));
maxV = max(max(BackI));
axis equal;
set(gca,'Ydir','Normal');
xlabel('y');
ylabel('z');
xlim([-1,1]);
ylim([-1,1]);
% caxis([0.5*maxV,maxV]);
title(['Reconstructed slice at y = ',num2str(0)]);


%
subplot(244);
[~,index] = min(abs(yAxis-yMomentum));
projectMapNew = squeeze(projectMap(:,:,index));
%filter the projection
projectMapFilt = projfilter(projectMapNew);
imagesc(angleAxis,zAxis,(projectMapFilt)');
% axis equal;
set(gca,'Ydir','Normal');
xlabel('y');
ylabel('z');
ylim([-1,1]);
title(['Rotate at y = ',num2str(yMomentum)]);

subplot(248);
[~,index] = min(abs(yAxis-yMomentum));
data = projectMapFilt;
BackI = back(data', angleAxis);
angleAxis;
% imagesc(yAxis,zAxis,(abs(BackI)));
imagesc(yAxis,zAxis,((BackI)));
maxV = max(max(BackI));
axis equal;
set(gca,'Ydir','Normal');
xlabel('y');
ylabel('z');
xlim([-1,1]);
ylim([-1,1]);
% caxis([0.5*maxV,maxV]);
title(['Reconstructed slice at y = ',num2str(yMomentum)]);
 