%project 3D momentum distribution on to YZ plane
function binData = project2D(source,xAxis,yAxis,noiseLevel)
    xData = source(2,:);
    yData = source(3,:);
    xMin = min(xAxis);
    xStep = xAxis(2)-xAxis(1); 
    ix = floor(xData/xStep - xMin/xStep + 0.5) + 1;
    yMin = min(yAxis);
    yStep = yAxis(2)-yAxis(1); 
    iy = floor(yData/yStep - yMin/yStep + 0.5) + 1;
    p = ix>0 & iy>0 & ix<=numel(xAxis) & iy<=numel(yAxis);
    pars = 1:numel(xData);
    pars = pars(p);
    binData = zeros(numel(yAxis),numel(xAxis));
    for p_index = 1:numel(pars)
        p = pars(p_index);
        binData(iy(p), ix(p)) = binData(iy(p), ix(p)) + 1;
    end
    %introduce effiency map
    if(1)
        effMap = ones(numel(yAxis),numel(xAxis));
        
%         radius = 10;
%         effMap(201-radius:201+radius,201-radius:201+radius) = 0;

        temp = linspace(0,1,401);
        
        effMap = repmat(temp',[1,401]);
        size(effMap)
        
        binData = binData.*effMap;
    
    end
    %introduce noise
    binData = binData + noiseLevel*randn(length(xAxis),length(yAxis));
    binData(find(binData<0)) = 0;