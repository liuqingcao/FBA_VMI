
%create the 3D momentum map
function source = create3DMomentum(samples,theta,theta_sigma,phi,phi_sigma,r,r_delta)
    %random numbers
    thetaList = Gauss(0,theta_sigma,samples);
    phiList = Gauss(phi,phi_sigma,samples);
    if(r_delta>0)
        rList = r*exprnd(r_delta,[samples,1]);
    else
        rList = r;
    end
    %convert to Cartesian coordinate system
    source(1,:) = rList.*sind(thetaList).*cosd(phiList); 
    source(2,:) = rList.*sind(thetaList).*sind(phiList);
    source(3,:) = rList.*cosd(thetaList);
    %rotate by theta and phi
    source = (source' * roty(theta,'deg'));
    
%     figure;
%     subplot(131);
%     hist(thetaList);
%     subplot(132);
%     hist(phiList);
%     subplot(133);
%     hist(rList);


function output = Gauss(xo,sigma,samples)
    r = sqrt(-2.0.*(sigma^2).*log(rand(samples,1)));
    phi = 2.0.*pi.*rand(samples,1);
    output = xo+r.*cos(phi);