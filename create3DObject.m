function source = create3DObject

%sphere
x0 = 0;
y0 = 0;
z0 = 0;
r0 = 0.3;
r1 = 0.31;
num0 = 500000;

rvals = 2*rand(num0,1)-1;
elevation = asin(rvals);
azimuth = 2*pi*rand(num0,1);
radii = r0 + (r1-r0)*(rand(num0,1).^(1/3));
[x,y,z] = sph2cart(azimuth,elevation,radii);
source = [x,y,z];
size(source)

% %rectangle
% x0 = 0;
% y0 = 0;
% z0 = 0;
% r0 = 0.6;
% num0 = 100000;
% rdm = rand(3,num0)-0.5;
% x = x0 + rdm(1,:)*r0;
% y = y0 + rdm(2,:)*r0;
% z = z0 + rdm(3,:)*r0*1.5;
% source = [x',y',z'];



